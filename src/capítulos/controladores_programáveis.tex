\chapter{Controladores Programáveis}
Nas seções a seguir, serão desenvolvidos conceitos a cerca de controladores programáveis, unidades remotas e protocolos de comunicação.


\section{Controladores Lógicos Programáveis}
Segundo a \textit{International Electrotechnical Commission} (IEC), um controlador lógico programável é definido como:

\begin{citacao}
 Um sistema eletrônico digital, desenvolvido para uso em um ambiente industrial, que utiliza uma memória programável para o armazenamento de instruções orientadas pelo usuário para implementação de funções específicas, tais como: lógica, sequenciamento, temporização, contadores e aritmética, para controlar, por meio de entradas e saídas digitais, diversos tipos de máquinas ou processos.O controlador programável e seus periféricos associados são projetados para serem facilmente integrados em um sistema de controle industrial e facilmente utilizados em todas suas funções desejadas \cite[tradução nossa]{iec14b}.
\end{citacao}

Em outras palavras, controlador lógico programável pode ser definido como um equipamento eletrônico de processamento que possui uma interface amigável com o usuário que tem como função executar controle de vários tipos e níveis de complexidade \cite[p.~25]{franchi:clp:2008}.

O objetivo de um CLP é, basicamente, apresentar uma determinada saída como resposta a uma entrada.
Para isso, o CLP executa em ciclo um programa determinado pelo usuário.
A Figura \ref{fig:plc_basico} representa graficamente esse ciclo.

\begin{figure}[htb]
    \centering
    \caption{\label{fig:plc_basico} Modelo Simplificado de um CLP}
    \begin{center}
        \includegraphics[width=10cm,scale=1]{imagens/plc_basico.eps}
    \end{center}
\end{figure}

A principal vantagem da utilização dos CLPs é que um único controlador pode ser utilizado para uma vasta gama de aplicações de sistemas de controle. E para modificar um sistema de controle existente, basicamente altera-se as instruções de controlador, em vez de modificar o sistema físico, o que trás maior flexibilidade com custo menor.

\subsection{Trajetória dos Controladores Programáveis}
Os CLPs surgiram a partir do desenvolvimento dos microcontroladores, iniciando uma nova geração de sistemas de controle.
A Figura \ref{fig:plc_history} representa a evolução dos sistemas de controle desde o século XIX até os dias atuais, onde é possível observar o abandono da utilização de sistemas de controle baseados em relés e situação no período atual.

\begin{figure}[htb]
    \caption{\label{fig:plc_history} Evolução dos sistemas de controle desde o final do século XIX}
    \begin{center}
        \includegraphics[width=15cm,scale=1]{imagens/plc_history.eps}
    \end{center}
    \legend{Fonte: Adaptado de \citeonline{franchi:clp:2008}}
\end{figure}

\subsection{Comparativo com outros Sistemas}
Os CLPs apresentam diversas vantagens. Em comparação a sistema baseados em relés, \citeonline[p.~27-28]{franchi:clp:2008} destacam-se:

\begin{alineas}
    \item Facilidade e flexibilidade para alterar os programas. O CLP pode ser reprogramado e operar com uma lógica distinta;
    \item O programa pode ser armazenado em memória para replicação em outro sistema ou ser guardado como reserva (\textit{backup});
    \item No caso de defeito, sinalizadores visuais no CLP informam ao operador a parte do sistema que está defeituosa.
\end{alineas}

No entanto, algumas questões relacionados ao uso dos CLPs devem ser consideradas, das quais devem ser destacadas:

\begin{alineas}
    \item Custo mais elevado;
    \item Uso de algum tipo de programação ou álgebra booleana no projeto, técnicas que são desconhecidas por uma boa parte dos eletricistas;
    \item Necessidade de maior qualificação da equipe de manutenção;
    \item Sensibilidade à interferência e ruídos elétricos, comuns em instalações industriais.
\end{alineas}

\subsection{Hardware}
Apesar do \textit{hardware} de um CLP variar, geralmente adequando-se a necessidades específicas, algumas características são comuns, podendo citar os seguintes componentes básicos: unidade de processamento (CPU), memória, unidade de alimentação, interfaces de entrada e saída, portas de comunicação e um dispositivo de programação \cite[p.~10]{bolton:plc:2011}.

A Figura \ref{fig:plc_hardware} representa o esquema básico do \textit{hardware} de um CLP.

\begin{figure}[htb]
    \caption{\label{fig:plc_hardware} \textit{Hardware} básico de um CLP}
    \begin{center}
        \includegraphics[width=16cm,scale=1]{imagens/plc_hardware.eps}
    \end{center}
    \legend{Fonte: Adaptado de \citeonline{bolton:plc:2011}}
\end{figure}

Analisando a Fig. \ref{fig:plc_hardware} é possível constatar que um CLP pode ser divido em seis partes \cite[p.~10-11]{bolton:plc:2011}, que são descritas a seguir:

\begin{alineas}
    \item \textbf{Unidade de Processamento}: é o local que contém o microprocessador. Ela executa as instruções básicas do sistema, o programa escrito pelo operador, carregado da memória, e o realiza o gerenciamento dos processos, enviando as decisões aos módulos de saída;
    
    \item \textbf{Fonte de Alimentação}: é responsável pelo fornecimento de energia elétrica para a alimentação do sistema. A forma como isso é feito, varia dependendo do fabricante e da finalidade do dispositivo. 
    
    \item \textbf{Dispositivo de Programação}: é utilizado para alimentar a memória do controlador com o programa que será executado. A forma como isso é feito varia de acordo com a tecnologia utilizada e fabricante.
    
    \item \textbf{Memória}: é a unidade que contém as instruções que serão executadas pela CPU. Além disso, é onde são armazenadas as informações obtidas em tempo de execução, tais como os valores de entrada.
    
    \item \textbf{Entradas e Saídas}: são os módulos responsáveis pela interface entre o mundo real e a CPU do controlador, utilizando as leituras desses módulos como valores de entrada para o programa, enquanto as saídas representam o resultados do algoritmo enviados para o meio externo. Geralmente, os controladores possuem entradas e saídas (E/S) analógicas e digitais.
    
    \item \textbf{Interface de Comunicação}: é utilizada para troca de informação com outros dispositivos por meio de protocolos de comunicação específicos.
\end{alineas}


\subsection{Linguagens de Programação}
Nas últimas décadas, observou-se um grande avanço nas linguagens de programação. Novos métodos, ferramentas e linguagens foram criadas, tornando a programação de computadores mais ágil e fácil.

Da mesma forma, a programação de CLPs também evoluiu e é inegável que atualmente é mais simples e flexível do que antes. Até então, a inexistência de normas em relação às linguagens de programação dos CLPs fez surgir diversas incompatibilidades entre dispositivos de fabricantes diferentes \cite[p. 96]{franchi:clp:2008}.

Para resolver essa questão, no decorrer da década de 1990, a IEC estabeleceu a norma IEC~61131, visando padronizar os CLPs modernos. Dentro desse padrão, o IEC~61131-3 define a diretriz para programação de CLPs, onde foram padronizados cinco métodos de programação \cite[p.~39]{john2010iec}, que são descritas a seguir:

\begin{alineas}
    \item \textbf{\textit{Sequential Function Chart} (SFC)}: originado com base nas redes de Petri e no Grafcet, descreve o fluxo do programa de forma a descrever o problema de controle em partes que pode ser executadas sequencialmente ou paralelamente.
    
    \item \textbf{\textit{Ladder} (LD)}: o diagrama \textit{Ladder} é uma linguagem gráfica baseada no diagrama elétrico de contatos.
    
    \item \textbf{\textit{Function Block Diagram} (FBD)}: diagrama de blocos funcionais é linguagem gráfica, baseada em circuitos lógicos.
    
    \item \textbf{\textit{Instruction List} (IL)}: lista de instruções é uma linguagem textual de baixo nível baseado em \textit{assembly}.
    
    \item \textbf{\textit{Structured Text} (ST)}: texto estruturado é uma linguagem textual de alto nível, similar a Pascal, recomendada para controle de tarefas que incluem cálculos mais complexos.
\end{alineas}

Deve-se ressaltar que não é obrigatório a definição de todas, mas, pelo menos uma deve ser implementada para a conformidade com padrão \cite[p.~11]{Logix5000}. Além disso, outras linguagens podem ser utilizadas, desde que suportem os mesmos tipos de dados do IEC~61131-3.

\subsection{Princípio de Funcionamento} \label{sec:principio_funcionamento}
O princípio de funcionamento de um CLP é baseado num sistema microprocessado em que  uma estrutura de \textit{software} realiza continuamente ciclos de leitura e escrita, chamado de ciclo de \textit{scan} ou ciclo de varredura.

A Figura \ref{fig:plc_fluxograma} (na página seguinte) apresenta um fluxograma que representa o algoritmo básico de um CLP genérico, demonstrando as principais etapas executadas durante o funcionamento de controlador programável.

Deve-se ressaltar que o algoritmos utilizados pelos CLPs podem variar de acordo com a aplicação para a qual o produto foi projetado. Por exemplo, a forma como o um erro é detectado pode variar entre dispositivos diferentes.

\pagebreak

\begin{figure}[h!]
    \caption{\label{fig:plc_fluxograma} Fluxograma de funcionamento do CLP.}
    \begin{center}
        \includegraphics[width=10cm,scale=1]{imagens/plc_fluxograma.eps}
    \end{center}
    \legend{Fonte: Adaptado de \citeonline{franchi:clp:2008}}
\end{figure}

\subsection{Ciclo de Varredura}
Segundo \citeonline{franchi:clp:2008}, durante o processo de \textit{scan} de um CLP básico, a CPU endereça os valores de entrada, coletando os dados em forma digital. Os valores lidos são armazenados em uma Tabela de Imagem das Entradas (TIE).

No processo de execução da lógica programada pelo usuário, a TIE é utilizada para obter os estados das entradas em nível de \textit{software}. Os resultados das lógicas programadas que atuam em determinadas saídas são armazenados na  Tabela de Imagem das Saídas (TIS).

Se durante a execução da lógica programada for necessária a referência a uma saída qualquer, dentro do mesmo ciclo de varredura, essa tabela é consultada. Deve-se ressaltar que durante esse processo não é feita nenhuma alteração aos pontos externos de E/S. A CPU trabalha somente com informações obtidas da memória.

Na etapa de atualização de saídas, a CPU executa uma varredura na TIS e atualiza as saídas externas por meio do endereçamento do sistema de E/S para atualizar o estado dos dispositivos de saída de acordo com o programa. Também é feita atualização de valores de outros operandos, como resultados aritméticos, contagens, temporizadores, entre outros. A partir daí é iniciado um novo \textit{scan} e a operação continua enquanto se mantém o controlador no modo de execução.

\section{Unidades Terminais Remotas}
Uma Unidade Terminal Remota ou \textit{Remote Terminal Unit} (RTU), é um dispositivo eletrônico utilizado para a aquisição de dados e controle.
Geralmente baseados em microprocessadores, são capazes de monitorar e controlar equipamentos em locais remotos, transferindo e recebendo informações de uma estação de controle \cite[p.~19]{clarke2004}.

\subsection{Hardware}
Em termos de hardware, uma remota básica possui um processador de controle e memórias associadas, E/S analógicas, E/S digitais, contadores, módulo de alimentação e uma interface de comunicação.

Em comparação a um CLP, as RTU são bastante semelhantes, estando na unidade de processamento e no  dispositivo de programação as principal diferenças. Isso deve-se ao fato de que as unidades remotas não precisam executar um programa, estando subordinadas às decisões de um controlador mestre. 

\subsubsection{Utilização de CLPs como Remotas} \label{sec:clp_n_rtu}
Os CLPS são comercialmente mais populares que as unidades remotas em função de sua versatilidade. 

Como possuem uma grande similaridade em termos de \textit{hardware} e, em alguns casos, podem realizar as mesmas tarefas, é possível utilizar um CLP como uma unidade remota.

No entanto, para alguns casos específicos, os CLPs não são aconselháveis, como, por exemplo, aplicações que exijam requerimentos especializados, como é o caso de aplicações para rádio telemetria \cite[p.~25]{clarke2004}.

\subsection{SCADA}
Segundo \citeonline[p.~16]{boyer2010}, \textit{Supervisory Control and Data Acquisition} (SCADA) é um sistema que permite o usuário coletar dados de uma ou mais instalações distantes e enviar instruções de controle limitadas para as mesmas, tornando desnecessário ao operador ter que se deslocar frequentemente para visitar tais locais, quando os dispositivos estão operando normalmente.

Uma das principais funções dos sistemas SCADA é fornecer uma interface para monitoramento dos dados medidos pelos equipamentos e permitir enviar comandos, quando necessário.

Os sistemas SCADA são capazes de interpretar diferentes protocolos de comunicação, levando os dados a um mesmo domínio de forma abstraída, onde podem ser manipulados pelo operador.

\section{Protocolos de Comunicação}
Quando em operação, é comum a necessidade de se trocar informações entre CLPs, RTUs ou outros  dispositivos. Seja para utilização em um processo ou para monitoramento, essa troca de informação deve ser realizada utilizando um protocolo de comunicação compatível para ambos os dispositivos envolvidos.

Atualmente, existem protocolos digitais industriais proprietários e abertos. Sendo que os protocolos proprietários surgiram primeiro, desenvolvidos por grandes fabricantes de sistemas completos de automação. Enquanto os protocolos abertos foram desenvolvidos por instituições internacionais, formada principalmente por associações de fabricantes \cite{Gutierrez2008}.

O surgimento dos protocolos abertos propiciaram a interoperabilidade entre dispositivos de diferentes fabricantes, além de beneficiar os desenvolvedores de porte menor, permitindo uma participação no mercado.

\subsection{Modelo OSI}
O modelo de referência \textit{Open Systems Interconnection} (OSI), foi desenvolvido com base em uma proposta desenvolvida pela \textit{International Organization for Standardization} (ISO). O modelo tem como objetivo padronizar a interconexão de sistemas abertos — ou seja, sistemas que estão abertos à comunicação com outros sistemas \cite{Tanenbaum2008}.

Deve-se ressaltar que o modelo OSI propriamente dito não é uma arquitetura de rede, pois não especifica os serviços e os protocolos exatos que devem ser usados em cada camada. Ele apenas informa o que cada camada deve fazer. No entanto, a ISO também produziu padrões para todas as camadas, embora esses padrões não façam parte do próprio modelo de referência. Cada um foi publicado como um padrão internacional distinto \cite{Tanenbaum2008}. 

\subsection{Camadas OSI} \label{sec:camadas_osi}
O modelo OSI apresenta sete camadas de referência para os protocolos de comunicação. A Figura \ref{fig:osi} (na página seguinte) representa graficamente as camadas OSI, as quais são descritas posteriormente:

\begin{figure}[htb]
    \caption{\label{fig:osi} As sete camadas do modelo OSI.}
    \begin{center}
        \includegraphics[resolution=300,width=12cm,scale=1]{imagens/osi.eps}
    \end{center}
    \legend{Fonte: Adaptado de \citeonline{clarke2004}}
\end{figure}

\begin{alineas}
    \item \textbf{Aplicação}: contém uma série de protocolos comumente necessários para os usuários \cite[p.~47]{Tanenbaum2008}.
    
    \item \textbf{Apresentação}: diferente das camadas mais baixas, que se preocupam principalmente com a movimentação de bits, a camada de apresentação está relacionada à sintaxe e à semântica das informações transmitidas. Para tornar possível a comunicação entre computadores com diferentes representações de dados, as estruturas de dados a serem intercambiadas podem ser definidas de maneira abstrata, juntamente com uma codificação padrão que será usada durante a conexão. Segundo \citeonline[p.~29]{Morimoto2011} essa também camada é utilizada em protocolos de criptografia, aumentando a segurança de comunicação de forma transparente, tanto para a aplicação quanto para o sistema operacional \cite[p.~47]{Tanenbaum2008}.
    
    \item \textbf{Sessão}: permite que os usuários de diferentes máquinas estabeleçam sessões entre eles. Uma sessão oferece diversos serviços, inclusive o controle de diálogo (mantendo o controle de quem deve transmitir em cada momento), o gerenciamento de símbolos (impedindo que duas partes tentem executar a mesma operação crítica ao mesmo tempo) e a sincronização (realizando a verificação periódica de transmissões longas para permitir que elas continuem a partir do ponto em que estavam ao ocorrer uma falha) \cite[p.~47]{Tanenbaum2008}. 
    
    \item \textbf{Transporte}: aceita os dados da camada acima dela, divide-os em unidades menores (caso necessário), repassa-as à camada de rede e assegura que todos os fragmentos chegarão corretamente à outra extremidade. Além disso, tudo isso deve ser feito com eficiência e de forma que as camadas superiores fiquem isoladas das inevitáveis mudanças na tecnologia de \textit{hardware} \cite[p.~47]{Tanenbaum2008}.
    
    \item \textbf{Rede}: controla a operação da sub-rede. Uma questão fundamental de projeto é determinar a maneira como os pacotes são roteados da origem até o destino \cite[p.~46]{Tanenbaum2008}.
    
    \item \textbf{Enlace}: transforma um canal de transmissão bruta em uma linha que pareça livre de erros de transmissão não detectados para a camada de rede \cite[p.~46]{Tanenbaum2008}. 
    
    \item \textbf{Física}: A camada física trata da transmissão de bits brutos por um canal de comunicação. O projeto da rede deve garantir que, quando um lado enviar um bit~1, o outro lado o receberá como um bit~1, não como um bit~0 \cite[p.~45]{Tanenbaum2008}.
\end{alineas}

\subsection{Principais Protocolos \textit{Ethernet}}
Recentemente, um grande número de sistemas de comunicação industrial vem sendo desenvolvido para uso de redes \textit{Ethernet} como base para os protocolos de controle com o objetivo de facilitar a integração da empresa. Contudo, esse uso ainda não foi consolidado, por limitação da \textit{Ethernet} e dificuldades na interoperabilidade entre equipamentos de fornecedores diferentes\cite{Gutierrez2008, Moore2014}.

De acordo com um relatório divulgado pela organização inglesa \textit{IMS Research} \cite[p.~4]{Moore2014} de 2012, intitulado  ``Crescimento do Uso de \textit{Ethernet} industrial'' (tradução~nossa), é possível observar quais são os principais protocolos de comunicação \textit{Ethernet} no mercado. A Figura \ref{fig:protocols} apresenta em um gráfico os tipo de rede com maior crescimento em 2012, pelo número de novas instalações. 

\begin{figure}[h]
    \caption{\label{fig:protocols} Crescimento dos Protocolos \textit{Ethernet} em 2012.}
    \begin{center}
        \includegraphics[width=16cm,scale=1]{imagens/protocols.eps}
    \end{center}
    \legend{Fonte: Adaptado de \textit{IMS Research (2012)}}
\end{figure}