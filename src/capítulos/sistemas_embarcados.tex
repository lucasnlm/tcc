\chapter{Sistemas Embarcados}
Nas seções a seguir, serão desenvolvidos breves conceitos a cerca de sistemas embarcados, focando-se na arquitetura ARM.

\section{Definição}
Um sistema embarcado, também chamado de sistema embutido, é um sistema microprocessado no qual o computador é completamente encapsulado ou dedicado a uma determinada tarefa. Diferentemente de computadores de propósito geral, como um computador pessoal (PC), um sistema embarcado realiza um conjunto de tarefas predefinidas, geralmente com requisitos específicos. Já que o sistema é dedicado a tarefas específicas, através de engenharia pode-se otimizar o projeto reduzindo tamanho, recursos computacionais e custo do produto \cite[p.~15-17]{Langbridge:2014:ARM}.

\section{Arquitetura ARM}
A \textit{Advanced RISC Machine} (ARM) é uma arquitetura de microprocessadores largamente utilizada em circuitos integrados (CI) embarcados, em dispositivos móveis, automóveis, equipamentos industriais, dentre outros. Em números, a ARM anunciou que desde até 2013 já foram produzidos a soma de 50 bilhões de \textit{chips} baseados nessa arquitetura \cite{ARM:Milestones}.

\subsection{Contexto Histórico}
Inicialmente conhecida como \textit{Acorn RISC Machine}, essa arquitetura é fruto do trabalho desenvolvido pela empresa inglesa \textit{Acorn Computer Group} durante a década de 1980. No início dessa década, a Acorn havia desenvolvido um microcomputador de bastante sucesso comercial para a rede de televisão \textit{British Broadcasting Corporation} (BBC), chamado BBC Micro, que utilizava um processador 6502 de 8~bits e 1~MHz, desenvolvido pela Rockwell, o mesmo utilizado em computadores populares na época tais como o Apple II, Commodore VIC-20, Atari 2600, dentre outros \cite[p.~14]{Levy:2013:Online}.

Além do BBC Micro, a Acorn desenvolveu diversos projetos baseados no 6502, o que levou a obterem bastante conhecimento sobre o \textit{hardware} daquele dispositivo. No entanto, o surgimento dos sistemas com interface gráfica acarretou a necessidade de se encontrar um sucessor para o 6502, pois esse não suportaria tanto processamento.

Diversos processadores disponíveis no mercado foram analisados, no entanto, nenhum atendeu a todos os requisitos, sendo, então, a única solução o desenvolvimento de um novo processador. 

Nesse novo rumo tomado pela empresa, a Acorn focou-se no desenvolvimento do projeto do microprocessador, deixando a fabricação do dispositivo nas mãos de outra empresa, a VLSI \textit{Technology}. Finalmente, em 1985 o ARM1 é finalizado. Um processador \textit{Reduced Instruction Set Computer} (RISC) de 32 bits, baseado no 6502 \cite{Prado:2013:Arm}.

No entanto, o sucesso dos computadores baseados em x86, que já possuía uma boa base de \textit{software} instalada, abalou o nicho de mercado dos computadores baseados em ARM. Mas, nessa época, já era visível o potencial de seus processadores em outro mercado: processadores RISC de alta desempenho, baixo consumo e voltados para sistemas embarcados \cite{Prado:2013:Arm}.

Em 1990, a partir de projetos conjuntos realizados entre a Acorn, VLSI e Apple, decidiu-se fundar uma nova empresa, a ARM \textit{Holdings} PLC, com o objetivo de aprimorar o desenvolvimento dos da arquitetura ARM \cite{ARM:Milestones}.

Atualmente, a ARM \textit{Holdings} não fabrica processadores. Seu diferencial, em relação a outros desenvolvedores é foco no \textit{design} da arquitetura. 

Enquanto as fabricantes normalmente desenvolvem processadores de uso geral e os disponibilizam no mercado, as empresas licenciadas pela ARM \textit{Holdings} podem produzir seus próprios processadores baseados na arquitetura ARM, adicionando os recursos que forem necessários e/ou otimizando-os para suas determinadas finalidades. O resultado são processadores com menor consumo e maior desempenho, quando comparados a processadores equivalentes de uso geral \cite[p.~34]{Langbridge:2014:ARM}.

\subsection{Características de Processadores ARM}
O processadores baseados na arquitetura ARM herdam a filosofia RISC, por isso, possuem características semelhantes a um processador RISC qualquer \cite[p.~121]{Langbridge:2014:ARM}. No entanto, apresentam alguns pontos específicos da plataforma. A seguir são listados as principais características dos processadores baseados em ARM: 

\begin{alineas}
    \item \textbf{Menos Instruções}: quando comparado a processadores de arquitetura \textit{Complex Instruction Set Computer} (CISC), os RISC possuem menor quantidade de instruções. Se tiverem sua execução otimizada, o sistema deve produzir resultadores de melhor desempenho, mesmo considerando que uma menor quantidade de instruções resultará em programas maiores \cite[p.~431]{Monteiro2001}.
    
    \item \textbf{Chamada de Funções Otimizada}: processadores CISC requerem mais tempo para a chamada de funções devido a necessidade de realizar operações de leitura e escrita com a memória. Os processadores RISC, no entanto, realizam essa tarefa apenas na CPU, o que permite obter um desempenho melhor na execução dos programas \cite[p.~431]{Monteiro2001}.
    
    \item \textbf{Menos Transistores}: os processadores RISC geralmente precisam de menos transistores que um CISC dedicados ao núcleo lógico do CPU. O espaço economizado permite aos fabricantes aumentar o tamanho do conjunto de registradores e o paralelismo interno \cite[p.~19]{Langbridge:2014:ARM}.
    
    \item \textbf{Processadores Menores e Mais Baratos}: como não necessitam de tantos transistores, os fabricantes podem desenvolver processadores menores, que consomem menos silício, e, consequentemente, produzir mais para um mesmo \textit{wafer}\footnote{Uma fina fatia de material semicondutor, geralmente silício, na qual microcircuitos são construídos pela dopagem.} \cite[p.~20]{Langbridge:2014:ARM}.
    
    \item \textbf{Uso de Linha de Montagem}: também chamado de \textit{Pipeline}, é uma técnica utiliza no \textit{design} dos processadores para aumentar o fluxo de instruções. Ao invés de ter que buscar uma instrução, decodificá-la e então executar de forma sequencial, a utilização de \textit{Pipeline} permite fazer os três durante um mesmo ciclo de \textit{clock} de forma paralela  \cite[p.~37]{Langbridge:2014:ARM}.
    
    \item \textbf{Coprocessadores}: os processadores ARM suportam coprocessadores, unidades secundárias de processamento, que podem manipular instruções enquanto o processador principal continua executando outras informações. \cite[p.~39]{Langbridge:2014:ARM}.

\end{alineas}

\section{Sistemas Operacionais}
A função de um sistema operacional (SO) é, sobretudo, gerenciar os recursos do sistema e abstrair a comunicação com o \textit{hardware}, permitindo o foco apenas no desenvolvimento do programa que será executado \cite[p.~2]{Tanenbaum2010}.

Atualmente, diversos SOs suportam ARM. Dentre eles, destacam-se: GNU/Linux, VxWorks, Android, iOS, Windows, dentre outros.

\subsection{Sistemas de Tempo Real}
Dentro de contexto dos SOs para sistemas embarcados, há um requisito que em alguns casos é necessário: os Sistemas Operacionais de Tempo Real (RTOS).

Os RTOSs são sistemas operacionais destinado à execução de múltiplas tarefas onde o tempo de resposta a um evento é pré-definido e constante. Esse tempo de resposta é chamado de prazo da tarefa e a perda de um prazo, isto é, o não cumprimento de uma tarefa dentro do prazo esperado, caracteriza uma falha do sistema \cite[p.~22]{Tanenbaum2010}. 

Nesse seguimento, existem diversos SOs que suportam plataformas baseadas em ARM. Dentre eles, destacam-se: FreeRTOS, VxWorks, QNX Neutrino, dentre outros.

\subsection{Tipos de RTOS}
Segundo \citeonline[p.~22]{Tanenbaum2010}, os RTOS podem ser classificados de acordo com nível de aceitação a erros, como é descrito a seguir:

\begin{alineas}
    \item \textbf{Crítico}: são aqueles em que as ações devem ocorrer necessariamente em determinados instantes. A não ocorrência da ação pode levar a consequências graves.
    
    \item \textbf{Não Crítico}: o descumprimento ocasional de um prazo, embora não desejável, é aceitável e não causa nenhum dano permanente.
\end{alineas}

\section{Linux Embarcado}
Um sistema operacional Linux refere-se a um SO que utiliza o núcleo Linux, inicialmente desenvolvido Linus Torvalds. Seu código fonte é disponibilizado sob a licença \textit{open source} \textit{GNU General Public License} (GPL) versão 2 \cite{gplv2}, o que permite que qualquer pessoa possa utilizar, estudar, modificar e distribuir livremente, desde que esteja de acordo com os termos da licença \cite[p.~448]{Tanenbaum2010}.

Segundo \citeonline[p.~9-14]{Yaghmour:2008:BEL:1461420} há diversas vantagens de utilizar um sistema Linux embarcado, sendo que alguns motivos se expandem a outros seguimentos tais como computadores \textit{desktop} e servidores, enquanto outros são restritos aos sistemas embarcados. Abaixo, são citadas algumas razões:

\begin{alineas}
    \item \textbf{Modularidade}: cada funcionalidade pode ser encontrada em um módulo separado, onde as funcionalidades mais complexas são subdividas em funções independentes.
    
    \item \textbf{Legibilidade}: o código pode ser lido e facilmente consertado.
    
    \item \textbf{Expansibilidade}: adicionar novas características ao código pode ser bastante simplificado.
    
    \item \textbf{Configurabilidade}: é possível definir quais partes do sistemas serão integradas ao produto final.
    
    \item \textbf{Portabilidade}: os sistemas baseados em Linux possuem um grande alcance de hardware. Mesmo quando um determinado hardware não é oficialmente suportado por seu fabricante, é comum encontrar \textit{drivers} livres desenvolvidos pela comunidade. O mesmo vale para quando o suporte é abandonado.
    
    \item \textbf{Comunidade}: o software livre é vantagem desse sistema. Dentre listas de \textit{e-mail} e fóruns a divulgação de informação sobre o assunto é extremamente grande.
    
    \item \textbf{Licença}: graças a liberdade dada pela licença do código é possível alterar o que desejar e redistribuir, desde que não quebre a licença do código original.
    
    \item \textbf{Preço}: diversas ferramentas de desenvolvimento e componentes do sistema operacional estão disponíveis sem custo algum. Dentre as quais, pode-se citar: Ångström, \textit{Embedded} Debian, Arch, Linaro, dentre outras.
\end{alineas}

\subsection{Arquitetura de um Sistema GNU/Linux}
É possível estruturar um sistema operacional GNU/Linux em dois níveis: o espaço do usuário ou de aplicação e o espaço do \textit{kernel} \cite{Jones2007}. A Figura \ref{fig:linux_layers} apresenta uma representação gráfica das camadas de um sistema GNU/Linux.

\begin{figure}[h!]
    \caption{\label{fig:linux_layers} Camadas Básicas de um Sistema GNU/Linux.}
    \begin{center}
        \includegraphics[width=15cm,scale=1]{imagens/linux_layers.eps}
    \end{center}
    \legend{Fonte: Adaptado de \citeonline{Jones2007}}
\end{figure}

\citeonline{Jones2007} descreve as camadas de um sistema GNU/Linux básico conforme se segue:

\begin{alineas}
    \item \textbf{Aplicação}: o espaço onde as aplicações do usuário é executado.
    \item \textbf{Biblioteca GNU C}: fornece a interface de chamada do sistema que se conecta ao \textit{kernel} e fornece o mecanismo para transição entre o aplicativo de espaço de usuário e o \textit{kernel}. Isso é importante, pois o \textit{kernel} e o aplicativo do usuário ocupam espaços de endereços diferentes e protegidos. E embora cada processo de espaço de usuário ocupe seu próprio espaço de endereço virtual, o \textit{kernel} ocupa um único espaço de endereço.
    \item \textbf{Interface de Chamada do Sistema}: implementa as funções básicas, como leitura e escrita, ligando o espaço do usuário e o espaço do \textit{kernel}.
    \item \textbf{\textit{Kernel}}: o núcleo do sistema, implementa vários atributos importantes da arquitetura. Em um nível avançado e em níveis mais baixos, o \textit{kernel} é dividido em camadas em diversos subsistemas distintos. O Linux pode também ser considerado monolítico porque agrupa todos os serviços básicos dentro do \textit{kernel}. 
    \item \textbf{\textit{Board Support Package}}: implementa o código específico para suporte a uma determinada placa, adaptando-a ao sistema operacional.
    \item \textbf{\textit{Hardware}}: a parte física do sistema, sendo específica para cada plataforma.
\end{alineas}


Dentro do \textit{kernel} alguns componentes internos devem ser destacados, conforme mostrados na Fig. \ref{fig:linux_layers}, são descritos a seguir:

\begin{alineas}
    \item \textbf{Gerenciador de Processos}: tem foco na execução de processos do sistema. No \textit{kernel}, eles são chamados de ``\textit{threads}'' e representam uma virtualização individual do processador. Enquanto, no espaço do usuário, são chamados de ``processos''. No entanto, a implementação Linux não separa os dois conceitos. O \textit{kernel} fornece uma interface de aplicação (API) com qual é possível criar, parar, comunicar e sincronizar processos, bem como gerencia a execução dos mesmos.
    
    \item \textbf{Gerenciador de Memória}: um importante recurso gerenciado pelo \textit{kernel} é a memória. Por questão de eficiência, a memória é gerenciada no que são chamadas páginas (de 4~KB na maioria das arquiteturas). O Linux inclui os meios para gerenciar a memória disponível, assim como os mecanismos de hardware para mapeamento físico e virtual. Esse esquema de gerenciamento de memória utiliza \textit{buffers} de 4~KB como base, mas, em seguida, aloca estruturas internamente, rastreando quais páginas estão completas, parcialmente usadas e vazias. Isso permite que o esquema aumente e diminua dinamicamente, com base nas necessidades do sistema geral. Ao oferecer suporte de memória a vários usuários, há ocasiões em que a memória disponível pode esgotar-se. Por isso, as páginas podem ser movidas da memória para o disco. Esse processo é chamado de troca porque as páginas são trocadas da memória para o disco rígido.
    
    \item \textbf{Sistema de Arquivos}: fornece uma abstração de interface aos sistemas de arquivos. Esse componente fornece uma camada de troca entre os sistemas de arquivos aos quais o \textit{kernel} oferece suporte.
    
    \item \textbf{Pilha de Rede}: é responsável pelo controle e intercâmbio entre redes de computadores, seguindo uma arquitetura em camadas modelada após os próprios protocolos, tais como o Protocolo de Internet (IP) e o do protocolo de transporte Protocolo de Controle de Transmissões (TCP).

    \item \textbf{\textit{Drivers} de Dispositivos}: é responsável por gerar a comunicação entre um dispositivo e o \textit{kernel}. A árvore de códigos-fonte do Linux fornece um subdiretório de \textit{drivers} que é posteriormente dividido pelos vários dispositivos suportados, dentre os quais pode-se citar: Bluetooth, I2C, serial, dentre outros.
\end{alineas}

\subsection{Processos e Prioridades}
Todos os computadores modernos são capazes de realizar diversas tarefas simultaneamente. Para isso, é necessário utilizar o conceito de processos.

Quando um sistema executa diversos processos simultaneamente com um único CPU, ele precisa dividir o processamento entre eles, o que é chamado de pseudoparalelismo. Da mesma forma, o verdadeiro paralelismo ocorre com sistemas multiprocessados (que tem duas ou mais CPUs que compartilham simultaneamente a mesma memória física) \cite[p.~50]{Tanenbaum2010}.

Nesse contexto, torna-se importe para o SO utilizar um escalonador de processos ou \textit{scheduling}, que possibilita executar os processos mais viáveis e concorrentes priorizando determinados tipos de processos.

\subsection{\textit{Bootloader}}
Um \textit{bootloader} é um programa que carrega o sistema operacional principal de um computador. Durante a inicialização do sistema, o \textit{bootloader} carrega para o sistema operacional para a \textit{Random Access Memory} (RAM), que geralmente é armazenado em um memória estática.

Atualmente, existem diversos programas \textit{Bootloader}, destacando-se os seguintes: LILO, ELILO, GRUB, SYSLINUX, \textit{Das} U-Boot, dentre outros. O principal diferencial entre eles é a plataforma alvo.

\subsubsection{Inicialização de um Sistema ARM}
O processo executado pelo \textit{bootloader} para a inicialização de um sistema operacional GNU/Linux pode apresentar algumas variações dependendo da plataforma sobre a qual o sistema será executado.

\citeonline{Russell2002} descrevem os processos realizados para a inicialização de um sistema Linux sob a plataforma ARM:

\begin{alineas}
    \item \textbf{Instalação e Inicialização da RAM}: o \textit{bootloader} busca e inicializa toda a memória RAM que o \textit{kernel} utilizará para o armazenamento de informações voláteis no sistema.
    
    \item \textbf{Inicializa uma porta \textit{Serial} (opcional)}: o \textit{bootloader} pode inicializar e ativar uma porta \textit{serial} na plataforma alvo. Isso permite ao \textit{driver serial} automaticamente detectar qual porta deve ser utilizada para o console do \textit{kernel} (geralmente utilziado para propósitos de depuração ou comunicação).
    
    \item \textbf{Detectar o tipo de máquina}: o \textit{bootloader} detecta o tipo de máquina que o está executando, verificando se o \textit{kernel} pode administrá-la.
    
    \item \textbf{Instalação de dados de \textit{boot}}: durante esse processo são carregadas dos dados de \textit{boot}, dentre eles é carregado a \textit{device tree}, ou árvore de dispositivos, que descreve o \textit{hardware} que está executando o sistema.
    
    \item \textbf{Descompactação da Imagem do \textit{kernel}}: em alguns casos, a imagem do sistema é armazenada em formato compactado, tornando necessário a descompactação antes de carregá-la.
    
    \item \textbf{Chama o \textit{kernel}}: o \textit{bootloader} chama o \textit{kernel}, inicializando o sistema operacional e, posteriormente, a aplicação do usuário.
\end{alineas}

\subsection{\textit{Watchdog}}
O cão de guarda, também conhecido pelo termo inglês \textit{watchdog}, é um tipo de temporizador utilizado como sistema de proteção de um sistema embarcado para situações anômalas, verificando se o tempo das repostas está dentro do esperado \cite[p.~143]{white2011making}. Em um sistema embarcado que não pode parar, um simples travamento deve ser tratado o mais rápido possível, por isso é importante a implementação de um temporizador \textit{watchdog}.

Em situações em que o usuário pode influenciar a execução do dispositivo a possibilidade de ocorrer travamentos é maior e deve ser prevenida pois, mesmo que o usuário tenha experiência, eventualmente, pode cometer erros. Em função disso, um temporizado \textit{watchdog} é indispensável.

\subsubsection{Funcionamento de um \textit{Watchdog}}
O temporizador \textit{watchdog} é inicializado imediatamente após o início do processamento das instruções de controle escritas pelo usuário. Nesse momento, o \textit{watchdog} inicia uma contagem regressiva a partir de um valor definido pelo desenvolvedor.

Para que o sistema continue em execução o \textit{watchdog} deve ser realimentado dentro do intervalo de sua contagem, antes contador zerar \cite{Barr:1998:PES:552624}. Caso não ocorra, o temporizador \textit{watchdog} acionará o circuito de reinicialização do sistema, considerando que houve um atraso maior que o tolerado na execução de seu programa principal. A Figura \ref{fig:watchdog} representa graficamente o relacionamento básico entre o temporizador \textit{watchdog} e o CPU.

\begin{figure}[h!]
    \caption{\label{fig:watchdog} Funcionamento Básico de um \textit{Watchdog}.}
    \begin{center}
        \includegraphics[width=10cm,scale=1]{imagens/watchdog.eps}
    \end{center}
\end{figure}

Deve-se ressaltar que o temporizador \textit{watchdog} não é uma solução preventiva para possíveis erros de código. Ao contrário, o \textit{watchdog} é acionado apenas quando a situação do sistema está irrecuperável, devendo ser reiniciado para tentar restabelecê-lo \cite[p.~143]{white2011making}. Por isso, o desenvolvedor deve tratar os possível erros de execução sempre que necessário, deixando o \textit{watchdog} como recurso final.

\subsection{\textit{Toolchains}}
No desenvolvimento para sistemas embarcados é comum utilizar um computador de outra arquitetura para compilar o código para uma outra arquitetura alvo. Esse processo é chamado de \textit{cross-compilation}. A razão de se fazer isso é, na maioria dos casos, agilizar o processo de desenvolvimento.

Dentro desse contexto, destaca-se a GNU \textit{toolchain}, que é essencial para o desenvolvimento para sistemas GNU/Linux, sendo descrita detalhadamente a seguir:

\begin{alineas}
    \item \textbf{GNU \textit{Binutils}}: possui as ferramentas necessárias para transformar o código \textit{assembly} gerado em linguagem de máquina da plataforma alvo. Além de uma ferramenta \textit{linker}, utilizada para ligar os objetos gerados pelo compilador em um único executável ou biblioteca.
    
    \item \textbf{GNU \textit{Compiler Collection} (GCC)}: é uma coleção de compiladores utilizados para gerar códigos compatíveis com uma determinada plataforma alvo. Atualmente, a GCC suporta as seguintes linguagens de programação: C, C{}\verb!++!, Java, Ada, Fortran e Objective-C.
    
    \item \textbf{Biblioteca C}: implementa a API de \textit{Portable Operating System Interface} (POSIX), com a qual é possível desenvolver aplicações no nível do espaço do usuário comunicando-se com os níveis mais baixos do \textit{kernel}. Dentre as implementações dessa biblioteca estão: glibc, \textit{Embedded} GLIBC e uClibc.
\end{alineas}
