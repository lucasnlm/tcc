REMOVENDO O APACHE

sudo service apache2 stop
sudo apt-get purge apache2 apache2-utils apache2.2-bin apache2-common
sudo apt-get autoremove

-- Deve retorna vazio:
-- which apache2 

-- Deve retornar: apache2: unrecognized service
sudo service apache2 start



REMOVENDO SERVI�OS IN�TEIS

systemctl disable bonescript.service
systemctl disable bonescript.socket
systemctl disable bonescript-autorun.service
systemctl disable cloud9.service                  
systemctl disable gateone.service    
systemctl disable avahi-daemon.service
systemctl disable gdm.service

VERIFICAR PORTAS

netstat -nl | grep tcp


REMOVER O NODEJS

apt-get autoremove
apt-get remove nodejs-dev
apt-get remove node-gyp
apt-get remove nodejs-legacy


REMOVENDO O GNOME

apt-get autoremove gdm3
apt-get autoremove --purge gnome*


VERIFICAR MEM�RIA RAM LIVRE

free -m

LISTAR PROGRAMAS RODANDO E MEM�RIA

ps aux --sort -rss

